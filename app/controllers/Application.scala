package controllers

import play.api.libs.json.Json
import play.api.mvc._

class Application extends Controller {

  def index = Action {
    Ok(views.html.main())
  }

  def ok(key: String, value: String) = {
    Json.obj(key -> value)
  }

  def square(a: Int) = a * a


}
