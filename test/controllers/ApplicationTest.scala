package controllers

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import play.api.libs.json.Json
import play.api.mvc.Results
import play.api.test.FakeRequest

import scala.concurrent.Await
import scala.concurrent.duration._

/**
  * Created by broilogabriel on 05/09/16.
  */
class ApplicationTest extends FlatSpec with Matchers {

  "Index" should "return ok" in {
    val ret = Await.result(new controllers.Application().index(FakeRequest()), 10.seconds)
    ret.header should be(Results.Ok.header)
    ret.body.contentLength.getOrElse(0) should not be 0
  }

  "Given key and value" should "create a json" in {
    new controllers.Application().ok("de", "boa").toString() should be(Json.obj("de" -> "boa").toString())
  }

  "A number" should "be multiplied by itself" in {
    new controllers.Application().square(2) should be(4)
  }

}
