name := """just-play-scala"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.8"

resolvers += Resolver.url("Typesafe Ivy releases", url("https://repo.typesafe.com/typesafe/ivy-releases"))(Resolver.ivyStylePatterns)

fork in run := true

libraryDependencies ++= Seq(
  ws,
  "org.scalactic" %% "scalactic" % "3.0.0",
  "org.scalatest" %% "scalatest" % "3.0.0" % "test"
)

lazy val root = project.in(file(".")).enablePlugins(PlayScala)
